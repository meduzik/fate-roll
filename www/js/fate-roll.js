const sheet_url = "https://docs.google.com/spreadsheets/d/1OeY7VgwvuX2KTR6ZXXILqZJJeoT0_RWTXScsvunUJX8/gviz/tq?tqx=out:csv";

const select_question_selector = document.getElementById("question_selector");
const div_question_magic_ball = document.getElementById("question_magic_ball");
const div_question_magic_ball_answer = document.getElementById("question_magic_ball_answer");

function sleep(time){
	return new Promise((resolve, reject) => {
		setTimeout(function(){
			resolve();
		}, time * 1000);
	});
}

function make_chooser(array){
	var last_i = null;
	function choose(){
		if (array.length < 2){
			return array[0];
		}	
		while (true) {
			var i = (Math.random() * array.length) | 0;
			if (i >= array.length) {
				i = array.length - 1;
			}
			if (i == last_i) {
				continue;
			}
			last_i = i;
			return array[i];
		}
	}
	return choose;
}

var active_roll = null;
async function roll_magic_answer(table){
	var my_roll = {};
	active_roll = my_roll;
	
	var n = 10 + Math.random() * 8;
	var roll_time = 0.8 + Math.random() * 0.8;
	var old_t = 0;
	var chooser = make_chooser(table.answers);
	for (var i = 0; i < n; i++) {
		if (active_roll != my_roll) {
			return;
		}
		div_question_magic_ball_answer.textContent = chooser();
		var new_t = Math.pow(i / n, 1.5);
		await sleep(roll_time * (new_t - old_t));
		old_t = new_t;
	}
	div_question_magic_ball_answer.textContent = chooser();
}

async function load_magic_quotes(){
	var raw_csv = await (new Promise((resolve, reject) => {
		var request = new XMLHttpRequest();
		request.addEventListener("load", function(){
			resolve(request.responseText);
		});
		request.addEventListener("error", reject);
		request.addEventListener("abort", reject);
		request.responseType = "text";
		request.open("GET", sheet_url);
		request.send();
	}));
	var data = Papa.parse(raw_csv);
	var tables = [];
	var header_row = data.data[0];
	for (var i = 0; i < header_row.length; i++){
		// there is some header text
		if (header_row[i]){
			var answers = [];
			var table = {
				id: "q_" + i,
				title: header_row[i],
				answers: answers
			};
			for (var j = 1; j < data.data.length; j++) {
				var row = data.data[j];
				if (row) {
					var cell = row[i];
					if (cell){
						answers.push(cell);
					}
				}
			}
			if (answers.length > 0) {
				tables.push(table);
			}
		}
	}
	
	for (var i = 0; i < tables.length; i++){
		var table = tables[i];
		var option = document.createElement('option');
		option.setAttribute('value', table.id);
		option.textContent = table.title;
		select_question_selector.appendChild(option);
	}
	
	
	
	select_question_selector.addEventListener('change', function(){
		div_question_magic_ball_answer.textContent = "Нажми чтобы получить предсказание...";
	});
	
	div_question_magic_ball.addEventListener('click', function(){
		var table_index = select_question_selector.selectedIndex;
		if (!tables[table_index]) {
			return;
		}
		var current_table = tables[table_index];
		roll_magic_answer(current_table);
	});
	
	div_question_magic_ball_answer.textContent = "Выбери вопрос";
	
	return data;
}

function make_die(sides){
	var div_die = document.getElementById("die_" + sides);
	var div_text = document.getElementById("die_" + sides + "_text");
	
	var answers;
	if (sides == 2){
		answers = ["ОРЕЛ", "РЕШКА"];
	}else{
		answers = [];
		for (var i = 0; i < sides; i++){
			answers.push(i + 1);
		}
	}
	
	var active_roll = null;
	async function roll_die_answer(){
		var my_roll = {};
		active_roll = my_roll;
		
		var n = 10 + Math.random() * 8;
		var roll_time = 0.8 + Math.random() * 0.8;
		var old_t = 0;
		var chooser = make_chooser(answers);
		for (var i = 0; i < n; i++) {
			if (active_roll != my_roll) {
				return;
			}
			div_text.textContent = chooser();
			var new_t = Math.pow(i / n, 1.5);
			await sleep(roll_time * (new_t - old_t));
			old_t = new_t;
		}
		div_text.textContent = chooser();
	}
	
	div_text.textContent = answers[answers.length - 1];
	
	div_die.addEventListener('click', function(){
		roll_die_answer();
	});
}

window.addEventListener('load', function(){
	load_magic_quotes();
	make_die(2);
	make_die(4);
	make_die(6);
	make_die(10);
	make_die(12);
	make_die(20);
	make_die(100);
});