window.addEventListener('load', function(){
	var div_die = document.getElementById("dice");
	
	var dice_value = 0;
	
	var roller = null;
	
	function set_dice(val) {
		dice_value = val;
		div_die.setAttribute("class", "dice" + (dice_value + 1));
	}
	
	function preroll(prev) {
		var x;
		while (true) {
			x = (Math.random() * 4)|0;
			if (x != prev) {
				return x;
			}
		}
	}
	
	function roll_dice() {
		var my_roller = {};
		roller = my_roller;
		
		var rolls = 8 + Math.random() * 12 | 0;
		var total_roll_time = 400 + Math.random() * 400;
		var roll_id = 0;
		var prev_roll = 0;
		
		function final_roll() {
			var x = Math.random() * 360;
			if (x < 110) {
				return 0;
			} else if (x < 220) {
				return 1;
			} else if (x < 290) {
				return 2;
			} else {
				return 3;
			}
		}
		
		function finalize_roll() {
			set_dice(final_roll());
		}
		
		function roll_next() {
			if (my_roller != roller) {
				return;
			}
			if (roll_id == rolls) {
				finalize_roll();
			} else {
				set_dice(preroll(dice_value));
				var p = roll_id / rolls;
				var timeout = total_roll_time * Math.pow(p, 2) - prev_roll;
				prev_roll += timeout;
				roll_id++;
				setTimeout(roll_next, timeout);
			}
		}
		
		setTimeout(roll_next, 0);
	}
	
	div_die.addEventListener("click", function() {
		roll_dice();
	});
});
